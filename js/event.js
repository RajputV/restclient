var DB_VERSION = 3;

/**
 * Listen for app install, app update or chrome update.
 * In case of install, create database schema and set
 * initianal settings.
 * @param {Object} details
 */



chrome.app.runtime.onLaunched.addListener(function() {
    chrome.app.window.create('index.html', {
        'id': 'arcMainWindow',
        'minWidth': 1280,
        'minHeight': 800
    });
   
});



