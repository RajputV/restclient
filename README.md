RestClient

This extension is used to send HTTP request i.e. GET, POST, PUT and DELETE to server. You can also send parameter with POST and PUT request.

To send request:- 

1. Put the URL of the api on which you want to send request in URL text box.

2. Select a method among (GET, POST, PUT, DELETE). 

3. IF you select POST/PUT, you can also send parameter.

4. Click on Submit button to send the request.

Once the request is over, responseText and response, send by server will be visible.

If URL is not reachable then, 'page not found' message is shown.

All parameter send with URL are URL-encoded.